# README #

This theme will overload the base Fixja Ninja theme.

It is a fork of the [European Touch Championships theme](https://bitbucket.org/fixja/eft.fixja.ninja).

*Note: this is a submodule and needs to be updated into a parent project to take effect.*